package com.devcamp.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.product.model.CProduct;

public interface IProductRepository extends JpaRepository<CProduct, Long> {

}
